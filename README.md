# OpenMobility Interest Group

The openMobility interest group will drive the evolution and broad adoption of mobility modelling and simulation technologies. It accomplishes its goal by fostering and leveraging collaborations among members. It is the goal of this interest group to deliver a methodology and a framework of tools based on validated models, which are accepted as “standard tools” in industry applications and academia for mobility modelling and simulation. Eclipse Foundation projects related to the openMobility interest group will provide technical implementations.

You can read more https://projects.eclipse.org/interest-groups/openmobility and https://openmobility.eclipse.org/interest-group/

# About Eclipse Interest Groups

Eclipse Interest Groups are a light-weight association of Eclipse Foundation Members that share a common interest in a topic or domain (Participating Members) that inherit and rely upon the Eclipse Foundation’s overall governance, sufficient to enable the Participating Members to collaborate effectively.  All participants must comply with the agreements, policies and procedures of the Eclipse Foundation.  In particular and without limiting the foregoing, at all times participants in all Eclipse Interest Groups must conform to the Eclipse Foundation’s Intellectual Property and Antitrust Policies. You can read more about the requirements to participate and related governance in the formal Eclipse Interest Group Process via https://www.eclipse.org/org/collaborations/interest-groups/process.php.

# Participation

In order to participate in an Interest Group, an organization must execute the Eclipse Foundation Membership Agreement as well as the Eclipse Foundation Member Committer and Contributor Agreement (if not already in place) and to follow any other steps on how to participate that are defined by the Eclipse Foundation.


# Guiding Principles

All Interest Groups must operate in an open and transparent manner.

**Open** - Everyone participates with the same rules; there are no rules to exclude any potential collaborators which include, of course, direct competitors in the marketplace.

**Transparent** - Minutes, plans, and other artifacts are open and easily accessible to all.
Interest Groups, like all communities associated with the Eclipse Foundation, must operate in adherence to the Eclipse Foundation’s Code of Conduct.

# Materials

Interest Groups must produce agendas and minutes for all meetings. Interest Groups may, at their discretion, produce artifacts such as documents, whitepapers, architectures, blueprints, diagrams, presentations, and the like; however, Interest Groups **must not** develop software, software documentation, or specifications.

# License

See License File.  Interest Group content is subject to the CC by 4.0 International License.

# Public Mailing List

https://www.eclipse.org/lists/openmobility

# Interested in Joining or have questions about Eclipse Industry collaborations?

Please contact us collaborations@eclipse-foundation.org

